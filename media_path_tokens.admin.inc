<?php

/**
 * @file
 * Administrative pages for the Media Directory Path.
 */

/**
 * Build media_path_tokens_settings_form form.
 *
 * @param array $form_state
 *   Array containing the state of all form elements
 *
 * @return array
 *   The created form.
 */
function media_path_tokens_settings_form($form_state) {
  $form = array();
  $form[MEDIA_PATH_TOKENS_MEDIA_TOKENPATH]['media_path_tokens'] = array(
    '#type' => 'textfield',
    '#title' => t("Directory Path"),
    '#default_value' => variable_get('media_path_tokens', ''),
    '#size' => 65,
    '#maxlength' => 1280,
    '#element_validate' => array('token_element_validate'),
    '#after_build' => array('token_element_validate'),
    '#min_tokens' => 1,
  );
  $form[MEDIA_PATH_TOKENS_MEDIA_TOKENPATH]['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form[MEDIA_PATH_TOKENS_MEDIA_TOKENPATH]['token_help']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => 'all',
  );
  return system_settings_form($form);
}
